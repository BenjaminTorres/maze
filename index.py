from tkinter import *
from tkinter import filedialog
import tkinter
from reader.data_source.maps import ReaderMaps

main_root = Tk()
#main_root = tkinter.Tk()

def uploadFile():
    main_root = Tk()
    main_root.filename = filedialog.askopenfilename(initialdir="/", title="Select file",
                                               filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
    main_root.quit()
    return main_root.filename

def print_maze():
    reader = ReaderMaps()
    route = uploadFile()
    map = reader.read_map(route)
    flag = reader.validate_map(map)

    if flag == False:
        print("Tu mapa esta incompleto")

    counter_col = 0
    counter_row = 0

    for m in map:
        counter_row = counter_row + 1
        for s in m:
            counter_col = counter_col + 1

    counter_col = counter_col / counter_row

    print("Filas: ",counter_row)
    print("Columnas: ", counter_col)

    diferens = reader.return_numbers(map)
    print(diferens)
    label_row = Label(frame,text = "Filas:" + str(counter_row),bg="floral white")
    label_row.place(x=0,y=0)
    label_col = Label(frame,text = "Columnas" + str(counter_col),bg="floral white")
    label_col.place(x=0,y=20)

    label_mountain = Label(frame, text="Montain" + str(counter_col), bg="floral white")
    label_mountain.place(x=0, y=60)
    label_path = Label(frame, text="Path" + str(counter_col), bg="floral white")
    label_path.place(x=0, y=80)
    label_water = Label(frame, text="Water" + str(counter_col), bg="floral white")
    label_water.place(x=0, y=100)
    label_sand = Label(frame, text="Sand" + str(counter_col), bg="floral white")
    label_sand.place(x=0, y=120)
    label_forest = Label(frame, text="Forest" + str(counter_col), bg="floral white")
    label_forest.place(x=0, y=140)
    label_lava = Label(frame, text="Lava" + str(counter_col), bg="floral white")
    label_lava.place(x=0, y=160)

    root = tkinter.Tk()

    for r in range(0, int(counter_col)):
        for c in range(0, int(counter_row)):
            cell = Entry(root, width=10, fg="blue4", bg="gray80")
            cell.grid(row=r, column=c)
            if(r > counter_row):
                pass
            else:
                cell.insert(0, map[r][c])

    main_root.mainloop()

#Barra del menú
name = ""
menubar = Menu(main_root);
main_root.config(menu=menubar)
main_root.title("Maze chingon")
filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Open Maze",command = print_maze)
filemenu.add_separator()
#filemenu.add_command(label="Salir",command=main_root.quit)

#Marco del laberinto
frame = Frame(main_root,width=700, height=500)
frame.pack(fill="both",expand=1)
frame.config(cursor="pirate",bg="floral white",bd=20,relief="sunken")


menubar.add_cascade(label="File",menu=filemenu)
menubar.add_cascade(label="Help",menu=filemenu)

main_root.mainloop()



