import csv


class ReaderMaps(object):

    def read_map(self, file_path):
        rows = []

        with open(file_path) as File:
            reader = csv.reader(File)
            for row in reader:
                rows.append(row)
        return rows

    def validate_map(self,rows):
        total = 0
        new = 0
        for row in rows:
            new =  len(row)
            if total == 0:
                total = new
            elif new != total:
                return False

            for each_type in row:
                if each_type == '':
                    return False
                try:
                    integer = int(each_type)
                except ValueError:
                    return False
        print(rows)
        return True

    def return_numbers(self,map):
        numbers = []
        for m in map:
            for i in m:
                if i in numbers:
                    pass
                else:
                    numbers.append(i)
        return numbers
